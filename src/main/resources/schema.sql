CREATE TABLE IF NOT EXISTS GRIEVANCE
(
        grievanceId VARCHAR2(32) PRIMARY KEY NOT NULL,
        studentId VARCHAR2(12) NOT NULL,
     	studentName VARCHAR2(30) NOT NULL,
     	grievance VARCHAR2(200) NOT NULL,
     	grievanceRemarks VARCHAR2(200),
     	grievanceStatus VARCHAR2(10) NOT NULL,
        createdDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updatedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        createdBy VARCHAR2(30) NOT NULL,
        UpdatedBy VARCHAR2(30) NOT NULL
  );

package com.bec.workshop.project.grievances.controller;

import com.bec.workshop.project.grievances.domain.Grievance;
import com.bec.workshop.project.grievances.service.GrievanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/grievances")
@RestController
public class GrievanceController {
    @Autowired
    GrievanceService service;

    @RequestMapping("/{id}")
    public ResponseEntity getById(@PathVariable String id) {
        Grievance grievance = service.getById(id);
        if (grievance == null) {
            return new ResponseEntity<>("Grievance not found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity getAllGrievances(@RequestParam("status") String status) {
        if(status != null)
            return new ResponseEntity<>(service.getAll(status), HttpStatus.OK);
        else
            return new ResponseEntity<>(service.getAll(), HttpStatus.OK);

    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveGrievance(@RequestBody @Valid Grievance grievance) {
        service.save(grievance);
        return new ResponseEntity<>("Grievance Info created", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateGrievance(@PathVariable String id, @RequestBody Grievance grievance) {
        if (!grievance.getGrievanceId().equals(id)) {
            return new ResponseEntity<String>("Grievance with given id is not found", HttpStatus.BAD_REQUEST);
        }
        service.save(grievance);
        return new ResponseEntity<>("Grievance updated successfully", HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/approve/{id}", method = RequestMethod.PUT)
    public ResponseEntity approveGrievance(@PathVariable String id) {
        Grievance grievance = service.getById(id);
        if (grievance == null) {
            return new ResponseEntity<String>("Grievance with given id is not found", HttpStatus.BAD_REQUEST);
        }
        service.approve(grievance);
        return new ResponseEntity<>("Grievance updated successfully", HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/resolve/{id}", method = RequestMethod.PUT)
    public ResponseEntity resolveGrievance(@PathVariable String id) {
        Grievance grievance = service.getById(id);
        if (grievance == null) {
            return new ResponseEntity<String>("Grievance with given id is not found", HttpStatus.BAD_REQUEST);
        }
        service.resolve(grievance);
        return new ResponseEntity<>("Grievance updated successfully", HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteGrievance(@PathVariable String id) {
        Grievance grievance = service.getById(id);
        if (grievance == null) {
            return new ResponseEntity<String>("Grievance with given id is not found", HttpStatus.BAD_REQUEST);
        }
        service.delete(grievance);
        return new ResponseEntity<>("Grievance updated successfully", HttpStatus.NO_CONTENT);
    }
}

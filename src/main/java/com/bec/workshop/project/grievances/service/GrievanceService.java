package com.bec.workshop.project.grievances.service;

import com.bec.workshop.project.grievances.constants.GrievanceStatus;
import com.bec.workshop.project.grievances.domain.Grievance;
import com.bec.workshop.project.grievances.repository.GrievanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class GrievanceService {
    @Autowired
    GrievanceRepository repository;

    public Grievance getById(String id) {
        return repository.findById(id).get();
    }

    public void save(Grievance grievance) {
        if(grievance.getGrievanceId() == null) {
            String id = UUID.randomUUID().toString();
            grievance.setGrievanceId(id);
            grievance.setGrievanceStatus(GrievanceStatus.PENDING);
            grievance.setCreatedBy("Grievance-API");
            grievance.setCreatedDate(Timestamp.from(Instant.now()));
            grievance.setUpdatedBy("Grievance-API");
            grievance.setUpdatedDate(Timestamp.from(Instant.now()));
        }
        repository.save(grievance);
    }

    public List<Grievance> getAll() {
        return repository.findAll();
    }

    public List<Grievance> getAll(String status) {
        return repository.getGrievanceByStatus(status);
    }

    public void delete(Grievance grievance) {
        repository.delete(grievance);
    }

    public void approve(Grievance grievance) {
        grievance.setGrievanceStatus(GrievanceStatus.APPROVED);
        grievance.setUpdatedBy("Grievance-API");
        grievance.setUpdatedDate(Timestamp.from(Instant.now()));
        repository.save(grievance);
    }

    public void resolve(Grievance grievance) {
        grievance.setGrievanceStatus(GrievanceStatus.RESOLVED);
        grievance.setUpdatedBy("Grievance-API");
        grievance.setUpdatedDate(Timestamp.from(Instant.now()));
        repository.save(grievance);
    }

}

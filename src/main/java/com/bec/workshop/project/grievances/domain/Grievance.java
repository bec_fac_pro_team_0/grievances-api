package com.bec.workshop.project.grievances.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Grievance {
    @Id
    String grievanceId;
    @Column
    String studentId;
    @Column
    String studentName;
    @Column
    String grievance;
    @Column
    String grievanceRemarks;
    @Column
    String grievanceStatus;
    @Column
    Timestamp createdDate;
    @Column
    Timestamp updatedDate;
    @Column
    String createdBy;
    @Column
    String updatedBy;
}

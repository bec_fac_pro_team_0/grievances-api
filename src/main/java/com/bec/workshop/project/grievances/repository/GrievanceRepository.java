package com.bec.workshop.project.grievances.repository;

import com.bec.workshop.project.grievances.domain.Grievance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrievanceRepository extends JpaRepository<Grievance, String> {

    @Query(value = "select * from GRIEVANCE where GRIEVANCE_STATUS=(:status)", nativeQuery = true)
    public List<Grievance> getGrievanceByStatus(@Param("status") String status);
}

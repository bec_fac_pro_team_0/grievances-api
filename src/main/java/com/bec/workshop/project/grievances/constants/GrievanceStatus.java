package com.bec.workshop.project.grievances.constants;

public class GrievanceStatus {
    public static final String PENDING = "pending";
    public static final String APPROVED = "approved";
    public static final String RESOLVED = "resolved";
}

package service;

import com.bec.workshop.project.grievances.domain.Grievance;
import com.bec.workshop.project.grievances.repository.GrievanceRepository;
import com.bec.workshop.project.grievances.service.GrievanceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class GrievanceServiceTest {
    @Mock
    GrievanceRepository grievanceRepository;

    @InjectMocks
    GrievanceService grievanceService = new GrievanceService();


    @Test
    public void testGetAll() {
        List<Grievance> grievances = new ArrayList<>();
        grievances.add(new Grievance());
        grievances.add(new Grievance());
        when(grievanceRepository.findAll()).thenReturn(grievances);

        List<Grievance> result =  grievanceService.getAll();
        assert result.size() == grievances.size();

    }
}
